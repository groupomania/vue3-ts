# Groupomania Frontend w/ Vue 3 (TS)

[TOC]

## Requirements

To run this project, you will need:
 - [`yarn`](https://classic.yarnpkg.com/lang/en/docs/install)

## Setup

### Install dependencies

Make sure to install the dependencies:

```bash
yarn install
```

### Environment variables

Define the environment variables, relying on the `.env.template` file:

```bash
cp .env.template .env
```

Fill the placeholders with your own values.

#### Reference

| Variable        | Description                | Environment-dependent |
|-----------------|----------------------------|:---------------------:|
| `VITE_API_HOST` | Host of the backend server |           ✅           |

## Deployment

### Development Server

Start the development server on http://localhost:3001

```bash
yarn dev
```

### Production

Build the application for production:

```bash
yarn build
```
