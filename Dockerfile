FROM node:lts-alpine as build-stage

ENV VITE_API_HOST="http://localhost:3000"

WORKDIR /app

# === Install dependencies with frozen lockfile

COPY package.json .
COPY yarn.lock .
RUN yarn install --frozen-lockfile

# === Build the application

COPY . .
RUN yarn build

FROM nginx:stable-alpine as run-stage

COPY --from=build-stage /app/dist /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
