import { createStore } from "vuex";
import jwtDecode from "jwt-decode";

export default createStore({
  state() {
    return {
      user: {},
      token: "",
    };
  },
  getters: {
    getAuthorizationHeader(state): { authorization: string } {
      return { authorization: `Bearer ${state.token}` };
    },
  },
  mutations: {
    setToken(state, token: string): void {
      localStorage.setItem("token", token);
      state.token = token;
    },
    deleteToken(state): void {
      localStorage.removeItem("token");
      state.user = {};
      state.token = "";
    },
    initializeStore(state): void {
      const savedToken = localStorage.getItem("token");
      if (savedToken) {
        state.token = savedToken;
        state.user = (<any>jwtDecode(savedToken)).data;
      }
    },
  },
});
