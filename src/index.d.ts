export type User = {
  id: string,
  name: string,
  email: string,
  role: string,
  profilePicture: string,
  moderator: boolean,
};

export type Like = {
  userId: string,
  postId: string,
};

export type Post = {
  id: string,
  datetime: string
  author: User,
  authorId: string,
  commentTo?: Post,
  commentToId?: string,
  content: string,
  image: string,
  likes: Like[],
  comments?: Post[],
};
