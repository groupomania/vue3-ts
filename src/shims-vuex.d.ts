import type { Store, Commit } from "vuex";
import type { User } from "@/index";

interface myStore<T> extends Store<T> {
  commit: Commit;
}

declare module "@vue/runtime-core" {
  interface State {
    user: User;
    token: string;
  }

  interface ComponentCustomProperties {
    $store: myStore<State>;
  }
}
